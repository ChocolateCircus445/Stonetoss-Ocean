 <h1>Using the module</h1>

First, add the following to your code:

`const sto = require("./sto-module.js")`

Then, see below.

<h2>What most functions return</h2>

*  title: Title of the comic
*  description: Description of the comic. Script tags will be replaced with "[SCRIPT]" to protect the user.
*  publishDate: Date the comic was published (yyyy-mm-dd)
*  url: Url of the image file.
*  altText: Hover text. The scripts will be neutralized here, too. 



<h2>sto.latest()</h2>

Returns: Object

Gets the latest Stonetoss comic.

```
sto.latest()
  .then(function(res) {
    console.log(res.title);
  })
//As of 11-19-2019, logs "Socialjism"
```

<h2>sto.specific(comic)</h2>

Returns: Object

Gets a specific Stonetoss comic. `comic` can be either the extension to the comic or the comic name.

```
sto.specific("acquired-tastes")
  .then(function(res) {
    console.log(res.description);
  })
//Logs "*snifffff*"
```

<h2>sto.archives()</h2>

Returns: Array of Objects

* name: The name of the comic.

* link: The extension of the comic

Grabs a list of all Stonetoss comics to date.

```
sto.archives()
  .then(function(res) {
    var l = res.length - 1
    console.log(res[l].link);
  })
 //As of 11-19-2019, logs "socialjism"
```
