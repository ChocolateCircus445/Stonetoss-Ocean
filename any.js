const http = require("http");
const rp = require("request-promise");
const chalk = require("chalk");
const comicFromArg = process.argv.slice(2)[0];
function convert(comic) {
  var comicName = comic.split(" ").join("-").toLowerCase();
  var punctuation = "?/!@#$%^&*()[]{}|\\+=~`.,<>:;'\""
  for (let i of punctuation) {
    comicName = comicName.split(i).join("");
  }
  comicName = "http://stonetoss.com/comic/" + comicName + "/"
  rp(comicName)
    .then(function(html) {
      console.log(chalk.cyan("================"));
      console.log(chalk.bold("Stonetoss Ocean"));
      console.log(chalk.cyan("----------------"));
      console.log("Stonetoss is a Nazi")
      console.log(chalk.cyan("----------------"));
      console.log(comic);
      console.log(chalk.cyan("----------------"));
      var description = html.split("class=\"post-content\">")[1].split("<p>")[1].split("</p>")[0];
      console.log("Description: " + description);
      var publishDate = html.split("<time datetime=\"")[1].split("T")[0];
      console.log("Publish Date (yyyy/mm/dd): " + publishDate);
      var imgURL = html.split("<div id=\"comic\">")[1].split("<img src=\"")[1].split("?fit=")[0];
      console.log("URL: " + imgURL);
      var altText = html.split("<div id=\"comic\">")[1].split("alt=\"")[1].split("\" title=\"")[0];
      console.log("Alt Text: " + altText);
      console.log(chalk.cyan("----------------"));
      console.log("Created by u/CC445");
      console.log(chalk.cyan("================"));
    })
    .catch(function(err){
      console.error("This comic probably doesn't exist. Maybe try replacing the spaces with underscores?")
    })
}
convert(comicFromArg);
