#!/bin/bash
echo "Welcome to the Stonetoss Ocean installer!"
echo "You must have Node in order for this to work."
echo "Do you have Node? (y/n)"
ogDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
read n
if [ $n = "y" ]; then
  npm i "request"
  npm i "request-promise"
  npm i "chalk"
  mkdir ~/.st-ocean
  cd "$ogDir"
  cp -r latest.js ~/.st-ocean
  cp -r any.js ~/.st-ocean
  cp -r comicList.js ~/.st-ocean
  cp -r help.js ~/.st-ocean
  cp -r sto-module.js ~/.st-ocean
  cp -r sto-server.js ~/.st-ocean
  cp -r sto.sh ~/.st-ocean
  nl=$'\n'
  echo "${nl}alias st-ocean='~/.st-ocean/sto.sh'${nl}" >> ~/.bash_profile
  clear
  echo "Done! You can now use Stonetoss Ocean and see the commands by typing 'st-ocean h'"
  exit
else
  echo "Stonetoss Ocean requires Node to work properly. To install Node, go to:"
  echo "https://nodejs.org/en/download/package-manager/"
  exit
fi
