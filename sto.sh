#!/bin/bash
ogDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ~/.st-ocean
if [ "$1" = "l" ]; then
  node "latest.js"
elif [ "$1" = "a" ]; then
  node "any.js" "$2"
elif [ "$1" = "c" ]; then
  node "comicList.js"
elif [ "$1" = "s" ]; then
  node "sto-server.js" "$2"
else
  node "help.js"
fi
cd "$ogDir"
