@echo off
echo Stonetoss Ocean Updater
echo.
pause
echo Copying files...
echo.
copy "%~dp0\"latest.js %SystemRoot%\st-ocean
copy "%~dp0\"any.js %SystemRoot%\st-ocean
copy "%~dp0\"comicList.js %SystemRoot%\st-ocean
copy "%~dp0\"help.js %SystemRoot%\st-ocean
copy "%~dp0\"sto-module.js %SystemRoot%\st-ocean
copy "%~dp0\"sto-server.js %SystemRoot%\st-ocean
copy "%~dp0\"st-ocean.bat %SystemRoot%\st-ocean
echo Done copying files.
cls
echo Done updating!
pause
exit
