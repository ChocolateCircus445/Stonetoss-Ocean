@echo off
echo Stonetoss Ocean Installer
echo.
echo This requires Node in order to work. If you don't have it, download it at
echo nodejs.org
pause
cls
mkdir %SystemRoot%\st-ocean
if not exist %SystemRoot%\st-ocean goto noAdmin
echo Installing packages...
echo.
setx NODE_PATH %AppData%\npm\node_modules
npm i request -g
npm i request-promise -g
npm i chalk -g
echo Done installing packages.
echo Copying files...
echo.
copy "%~dp0\"latest.js %SystemRoot%\st-ocean
copy "%~dp0\"any.js %SystemRoot%\st-ocean
copy "%~dp0\"comicList.js %SystemRoot%\st-ocean
copy "%~dp0\"help.js %SystemRoot%\st-ocean
copy "%~dp0\"sto-module.js %SystemRoot%\st-ocean
copy "%~dp0\"sto-server.js %SystemRoot%\st-ocean
copy "%~dp0\"st-ocean.bat %SystemRoot%\st-ocean
echo Done copying files.
setx path "%path%;%SystemRoot%\st-ocean"
cls
echo Done! You can now use Stonetoss Ocean and see the commands by typing 'st-ocean h'
pause
exit

:noAdmin
cls
echo You need admin privileges to install Stonetoss Ocean.
echo Close this window, right click on "win-install", then click "Run as administrator."
pause
goto:eof
