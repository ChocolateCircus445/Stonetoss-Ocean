@echo off
echo If you want help, type "st-ocean h"
echo.
if %1==l (
  node "%~dp0\"latest.js
) else (
  if %1==a (
    node "%~dp0\"any.js
  ) else (
    if %1==c (
      node "%~dp0\"comicList.js
    ) else (
      if %1==s (
        node "%~dp0\"sto-server.js %2
      ) else (
        node "%~dp0\"help.js
      )
    )
  )

)
