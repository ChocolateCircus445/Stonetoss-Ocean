<h1>Stonetoss Ocean</h1>

This has been tested on Windows 10 and macOS Catalina.

**Prerequisites**

* Internet connection

* Node.js (grab a copy [here](http://nodejs.org))

* NPM (usually installed with Node.js)

<h2>Downloading the software</h2>

Click the green "Clone or download" button on the right side of the screen, just above the file listing, then click the "Download ZIP" button. When it's done downloading, extract the files with your favorite archive manager.

<h2>Windows</h2>

Right click on "win-install.bat" (or just "win-install" on some machines) and click on "Run as administrator." When it asks if you want to make changes to your machine, click Yes. After that, follow the on-screen instructions.

If you want to update, redownload the repository, then follow the steps above, but right click "win-update" instead of "win-install"

<h2>macOS</h2>

Drag "mac-install.sh" into a new Terminal window. Press enter, then follow the on-screen instructions.

<h2>Using the software</h2>

**NOTE:** On Windows, if you don't provide any arguments, it will say, "( was unexpected at this time." This is normal, and is just a result of somewhat sloppy coding.

Open Command Prompt or Terminal, and type `st-ocean`

(This can be done on Windows by pressing Windows Key and R at the same time, then entering "cmd" into the box. On Mac, open Launchpad, go into your Utilities folder, and click on Terminal.)

It should give you a list of commands.

**st-ocean l**

Fetches the latest comic

**st-ocean a \<comic name\>**

Fetches a specific comic, for example:

`st-ocean a "Acquired Tastes"` will fetch Acquired Tastes

**st-ocean c**

Fetches a list of all comics thus far.

**st-ocean s \<port number \(optional, defaults to 8080\)\>**

This starts the server that you can open in your browser. It will provide instructions to access it upon startup.
