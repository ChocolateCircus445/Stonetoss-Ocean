const request = require("request");
const rp = require("request-promise");
module.exports = {
  latest: function() {
    var res = {};
    return new Promise(function(resolve, reject) {
      rp("http://stonetoss.com")
      .then(function(html) {
        res.title = html.split("rel=\"bookmark\">")[1].split("</a>")[0];
        res.description = html.split("class=\"post-content\">")[1].split("<p>")[1].split("</p>")[0].split("<script>").join("[SCRIPT]").split("</script>").join("[SCRIPT]");
        //Prevent malicious scripts from running
        res.publishDate = html.split("<time datetime=\"")[1].split("T")[0];
        res.url = html.split("<div id=\"comic\">")[1].split("<img src=\"")[1].split("?fit=")[0];
        res.altText = html.split("<div id=\"comic\">")[1].split("alt=\"")[1].split("\" title=\"")[0].split("<script>").join("[SCRIPT]").split("</script>").join("[SCRIPT]");
        resolve(res);
      })
      .catch(function(err){
        reject(err);
      })
    })
  },
  specific: function(comic) {
    var res = {};
    var comicName = comic.split(" ").join("-").toLowerCase();
    var punctuation = "?/!@#$%^&*()[]{}|\\+=~`.,<>:;'\""
    for (let i of punctuation) {
      comicName = comicName.split(i).join("");
    }
    comicName = "http://stonetoss.com/comic/" + comicName + "/"
    return new Promise(function(resolve, reject) {
    rp(comicName)
      .then(function(html) {
        res.title = html.split(`<header class="post-header">`)[1].split("<h1>")[1].split("</h1>")[0];
        res.description = html.split("class=\"post-content\">")[1].split("<p>")[1].split("</p>")[0].split("<script>").join("[SCRIPT]").split("</script>").join("[SCRIPT]");
        //Prevent malicious scripts from running
        res.publishDate = html.split("<time datetime=\"")[1].split("T")[0];
        res.url = html.split("<div id=\"comic\">")[1].split("<img src=\"")[1].split("?fit=")[0];
        res.altText = html.split("<div id=\"comic\">")[1].split("alt=\"")[1].split("\" title=\"")[0].split("<script>").join("[SCRIPT]").split("</script>").join("[SCRIPT]");
        resolve(res);
      })
      .catch(function(err){
        console.error("This comic probably doesn't exist. Maybe try replacing the spaces with underscores?");
        reject(err);
      })
    })
  },
  archives: function() {
    var res = [];
    return new Promise(function(resolve, reject) {
    rp("http://stonetoss.com/archive/")
      .then(function(html) {
        var archiveText = html.split("<div class=\"comic-archive-list-wrap\">")[1].split("<div style=\"clear:both;\">")[0].split("</div>");
        for (let i = 0; i < archiveText.length - 2; i++) {
          let x = archiveText[i].split("Permanent Link: ")[1];
          let l = archiveText[i].split("http://stonetoss.com/comic/")[1].split("\"")[0];
          res.push({name: x.split("\">")[0], link: l});
          resolve(res);
        }
      })
      .catch(function(err) {
        reject(err)
      })
  })
  }
}
