#!/bin/bash
echo "Welcome to the Stonetoss Ocean Updater!"
ogDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$ogDir"
cp -r latest.js ~/.st-ocean
cp -r any.js ~/.st-ocean
cp -r comicList.js ~/.st-ocean
cp -r help.js ~/.st-ocean
cp -r sto-module.js ~/.st-ocean
cp -r sto-server.js ~/.st-ocean
cp -r sto.sh ~/.st-ocean
echo "Done updating!"
exit
